#include "list.h"
#include <stdio.h>
#include <stdlib.h>

list_t *alloc_cell(void *data) {
  list_t *cell = (list_t *)malloc(sizeof(list_t));
  cell->data = data;
  cell->next = NULL;
  return cell;
}

void dealloc_cell(list_t *cell) {
  free(cell->data);
  free(cell);
}

list_t *append(list_t **head, void *data) {
  if (!head) {
    *head = alloc_cell(data);
    return *head;
  }

  list_t *cur = *head;
  while (cur->next) {
    cur = cur->next;
  }
  cur->next = alloc_cell(data);
  return *head;
}

list_t *prepend(list_t *head, void *data) {
  if (!head) {
    return alloc_cell(data);
  }
  list_t *new_head = alloc_cell(data);
  new_head->next = head;
  return new_head;
}

