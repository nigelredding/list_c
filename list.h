#ifndef LIST_H
#define LIST_H

/*
 * List defn
 */
typedef struct list_t {
  int data;
  struct list_t *next;
} list_t;

/* standard fns */
list_t *alloc_cell(void *);
void dealloc_cell(list_t *);
list_t *append(list_t **, void *);
list_t *prepend(list_t *, void *);


/* extra */
void print_cell(list_t *);

#endif
