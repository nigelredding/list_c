// #include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct list_t {
  void *data;
  struct list_t *next;
} list_t;

void print_list(list_t *lis) {
  if (!lis) {
    printf("(null)\n");
  } else {
    printf("%s | ", lis->data);
    print_list(lis->next);
  }
}

void acell(list_t **lis, void *data) {
  if (!*lis) {
    *lis = (list_t *)malloc(sizeof(list_t));
    (*lis)->data = strdup(data);
    (*lis)->next = NULL;
    return;
  }
  list_t *cur = *lis;
  while (cur->next) {
    cur = cur->next;
  }
  cur->next = (list_t *)malloc(sizeof(list_t));
  cur->next->data = strdup(data);
  cur->next->next = NULL;
} 

int main(int argc, char **argv) {
  FILE *fd = fopen("in.txt", "r");

  list_t *doc = NULL;
  
  ssize_t bytes_read;
  char *line;
  size_t len;
  while ((bytes_read = getline(&line, &len, fd)) != -1) {
    line[bytes_read] = NULL;
    printf("line = %s\n", line);
    acell(&doc, line);
  }

  print_list(doc);
  
  return 0;
}
